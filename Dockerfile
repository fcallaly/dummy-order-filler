FROM openjdk:11-jre

COPY target/dummy-trade-filler-0.0.1-SNAPSHOT app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]
